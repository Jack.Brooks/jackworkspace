from fastapi import FastAPI
import random

app = FastAPI()

@app.get("/hello")

def api_hello():

    return {"message": "Hello everyone"}

@app.get("/addtwo")

def api_addtwo(nFirst: int, nSecond: int):

    n = nFirst + nSecond

    return{"result": n}



#unicorn [name of file without .py] :app --reload

#Example uvicorn addtwoapi:app --reload

#http://127.0.0.1:8000/hello

#http://127.0.0.1:8000/addtwoapi?nFirst=2&nSecond=3

